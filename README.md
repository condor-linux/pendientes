# Pendientes

## Region e idiomas
- [ ] Distribución del teclado (es_CO|es_Es) 
- [ ] Zona Horaria (UTC)

## Grub
- [ ] Personalizacion del tema del grub para arranque inicial

## KDE
- [ ] Crear el tema predeterminado para KDE

## Programas y herramientas
- [ ] Mejorar la seleccion de herramientas y programas de la distro

## Virtualizacion
- [ ] Solucionar los problemas de ejecucion en Virtualizacion (VBox)

## Drivers
- [ ] Drivers OpenSource Nvidia
- [ ] Drivers para antenas de red inalambrica externas (Wi-Fi)
- [x] Drivers de sonido

## Calamares
- [ ] Iniciar el rebranding del instalador Calamares (Usar solo el esquema BTRFS para instalacion en disco)
